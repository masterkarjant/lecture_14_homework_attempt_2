
# Docker Commands

docker images
docker container ls 
docker container ls -a // will show inactive containers

# Add Simple PostgresDB
docker run --name my-postgres --env POSTGRES_PASSWORD=pgpass --env POSTGRES_USER=pguser -p 5432:5432 -d postgres:14.5

docker exec -it my-postgres psql -U pguser // will connect to it