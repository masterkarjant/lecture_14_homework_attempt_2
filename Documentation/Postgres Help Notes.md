# POSTGRES psql Commands

\l              // List all available databases 
\c musicdb      // Connect to database named musicdb
\dt             // List all tables in the database  

DROP TABLE table_name; // to delete table