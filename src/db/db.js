import pg from 'pg'
import { createArtistsTable, createAlbumsTable } from './queries.js'

const isProd = process.env.NODE_ENV === 'production'
const { PG_HOST, PG_PORT, PG_USERNAME, PG_PASSWORD, PG_DATABASE } = process.env

export const pool = new pg.Pool({
    host: PG_HOST,
    port: PG_PORT,
    user: PG_USERNAME,
    password: PG_PASSWORD,
    database: PG_DATABASE,
    ssl: isProd
})

const executeQuery = async (query, parameters) => {
    const client = await pool.connect()
    try {
        const result = await client.query(query, parameters)
        return result
    }
    catch (error) {
        console.error(error.stack)
        error.name = 'dbError'
        return [];
        throw error
    }
    finally {
        client.release()
    }
}

export const initiateCreateArtistsTable = async () => {
    await executeQuery(createArtistsTable)
    console.log('Initiate Create Artists Table')
}

export const initiateCreateAlbumsTable = async () => {
    await executeQuery(createAlbumsTable)
    console.log('Initiate Create Albums Table')
}

export default executeQuery