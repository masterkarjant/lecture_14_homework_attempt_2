import { Router } from 'express'
import * as dao from '../db/dao.js'

const router = Router()

// - GET ALL

router.get('/artists', async (_req, res) => {
    console.log('GET artists')
    const artists = await dao.getAllArtists()
    res.send(artists)

})

router.get('/albums', async (_req, res) => {
    console.log('GET albums')
    const artists = await dao.getAllAlbums()
    console.log("THERE IS AN ISSUEEEE")
    res.send(artists)
    console.log("THERE IS AN ISSUEEEE")
})

// - GET ONE

router.get('/artists/:id', async (req, res) => {
    console.log('GET artist with id: ' + req.params.id)
    const artist = await dao.getTargetArtist(req.params.id)
    if (!artist)
        return res.send(404)

    res.send(artist)
})

router.get('/albums/:id', async (req, res) => {
    console.log('GET album with id: ' + req.params.id)
    const album = await dao.getTargetAlbum(req.params.id)
    if (!album)
        return res.send(404)

    res.send(album)
})

// - POST NEW

router.post('/artists', async (req, res) => {
    console.log('POST create new artist', req.body)
    const artist = await dao.insertTargetArtist(req.body)
    res.send(artist)
})

router.post('/albums', async (req, res) => {
    console.log('POST create new album', req.body)
    const album = await dao.insertTargetAlbum(req.body)
    res.send(album)
})

// - PUT 

router.put('/artists/:id', async (req, res) => {
    console.log('PUT update artist: ', { id: req.params.id, ...req.body })
    const artist = await dao.updateTargetArtist(req.params.id, req.body)
    if (!artist)
        return res.send(404)

    res.send(artist)
})

router.put('/albums/:id', async (req, res) => {
    console.log('PUT update album: ', { id: req.params.id, ...req.body })
    const album = await dao.updateTargetAlbum(req.params.id, req.body)
    if (!album)
        return res.send(404)

    res.send(album)
})

// - DELETE 

router.delete('/artists/:id', async (req, res) => {
    console.log('DELETE artist with id: ' + req.params.id)
    const artist = await dao.deleteTargetArtist(req.params.id)
    if (!artist)
        return res.send(404)

    res.send(artist)
})

router.delete('/albums/:id', async (req, res) => {
    console.log('DELETE album with id: ' + req.params.id)
    const album = await dao.deleteTargetAlbum(req.params.id)
    if (!album)
        return res.send(404)

    res.send(album)
})


export default router

